const app = require('./index');
const http = require('http');

describe('Test the root path', () => {
  test('It should respond to GET method with status code 200', async () => {
    const options = {
      hostname: 'localhost',
      port: 3000,
      path: '/',
      method: 'GET',
    };

    // Wrap the HTTP request code inside a Promise
    const response = await new Promise((resolve, reject) => {
      const request = http.request(options, (response) => {
        resolve(response);
      });

      request.end();
    });

    expect(response.statusCode).toBe(200);
  });

  test('It should respond with "Hello World"', async () => {
    const options = {
      hostname: 'localhost',
      port: 3000,
      path: '/',
      method: 'GET',
    };

    // Wrap the HTTP request code inside a Promise
    const data = await new Promise((resolve, reject) => {
      let responseData = '';

      const request = http.request(options, (response) => {
        response.on('data', (chunk) => {
          responseData += chunk;
        });

        response.on('end', () => {
          resolve(responseData);
        });
      });

      request.end();
    });

    expect(data).toBe('Hello World\n');
  });
  afterAll(() => {
  setTimeout(() => {
    process.exit(0);
  }, 3000); 
});

});